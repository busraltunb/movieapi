﻿using Hangfire;
using Microsoft.Extensions.Options;
using Movie.Core.Helpers;
using Movie.Data.Contexts;
using Movie.Data.Dtos.Api;
using Movie.Entity.Concrete.Movies;
using Movie.Hangfire.Abstract;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Text.Json;
using System.Threading.Tasks;

namespace Movie.Hangfire.Concrete
{
    public class JobTestService : IJobTestService
    {
        #region ctor
        private readonly IHttpClientFactory _httpClientFactory;
        private readonly Context _context;
        private readonly MovieDbInfo _movieDbInfo;
        public JobTestService(IHttpClientFactory httpClientFactory, Context context, IOptions<MovieDbInfo> movieDbInfo)
        {
            _httpClientFactory = httpClientFactory;
            _context = context;
            _movieDbInfo = movieDbInfo.Value;
        }

        #endregion


        public JobTestService()
        {
            RecurringJob.AddOrUpdate(() => ReccuringJob(), Cron.Hourly);
        }
        public async Task<object> ReccuringJob()
        {
            HttpClient httpClient = _httpClientFactory.CreateClient();
            httpClient.BaseAddress = new Uri(_movieDbInfo.Url);
            var uri = "upcoming?api_key=4f4107f0ab25c2318bfe2beacb0886e0&language=en-US&page=1";          
            var result = await httpClient.GetStringAsync(uri);

            MovieListDto movies = JsonSerializer.Deserialize<MovieListDto>(result);

            foreach (var movie in movies.results)
            {
                var findMovie = _context.Movies.FirstOrDefault(x => x.MovieId == movie.id);

                if(findMovie != null)
                {
                    findMovie.Title = movie.title;
                    findMovie.MovieNote = movie.overview;
                    findMovie.Value = (int)Math.Ceiling(movie.vote_average);

                    _context.Update(findMovie);
                    _context.SaveChanges();
                }
                else
                {
                    MovieEntity updateMovie = new MovieEntity()
                    {
                        MovieId = movie.id,
                        Title = movie.title,
                        MovieNote = movie.overview,
                        Value = (int)Math.Ceiling(movie.vote_average)
                    };

                    _context.Add(updateMovie);
                    _context.SaveChanges();
                }
            }

            return JObject.Parse(result);

        }
    }
}
