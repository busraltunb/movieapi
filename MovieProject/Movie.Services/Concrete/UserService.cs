﻿using Microsoft.Extensions.Options;
using Microsoft.IdentityModel.Tokens;
using Movie.Core.Helpers;
using Movie.Data.Contexts;
using Movie.Data.Dtos.UserDto;
using Movie.Entity.Concrete.User;
using Movie.Services.Abstract;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Net.Http;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;

namespace Movie.Services.Concrete
{
    public class UserService : IUserService
    {
        #region ctor
        private readonly Context _context;
        private readonly AppSettings _appSettings;

        
        public UserService(Context context, IOptions<AppSettings> appSettings) 
        {
            _context = context;
            _appSettings = appSettings.Value;
         }
        #endregion

        #region User Create
        public async Task<UserEntity> AddAsync(UserEntity userEntity)
        {
            
            await _context.AddAsync(userEntity);
            await _context.SaveChangesAsync();
            return userEntity;
        }

        #endregion

        #region Authenticate
        public UserLogin Authenticate(string UserNickName, string UserPassword)
        {
            var user= _context.Users.FirstOrDefault(x => x.UserNickName == UserNickName && x.UserPassword == UserPassword);

            if(user == null) return null; // Kullanici bulunamadıysa null döner.

            var tokenHandler = new JwtSecurityTokenHandler(); // Security Token Handler oluşturma

            var key = Encoding.UTF8.GetBytes(_appSettings.SecretKey); //Şifrelenecek Özel Anahtar Oluştur
            var tokenDescriptor = new SecurityTokenDescriptor // Jwt Açıklama
            {
                Subject = new ClaimsIdentity(new Claim[] { new Claim(ClaimTypes.Name, user.Id.ToString()) }),
                Expires = DateTime.UtcNow.AddDays(30),
                SigningCredentials = new SigningCredentials(new SymmetricSecurityKey(key),
                    SecurityAlgorithms.HmacSha256Signature)
            };
            var token = tokenHandler.CreateToken(tokenDescriptor); // Token oluşturma

            UserLogin userLogin = new UserLogin();
            userLogin.UserEntity = user;
            userLogin.UserToken = tokenHandler.WriteToken(token);

            return userLogin;
        }
        #endregion

    }
}
