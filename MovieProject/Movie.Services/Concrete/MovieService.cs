﻿using Autofac.Core;
using MailKit.Net.Smtp;
using MailKit.Security;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Options;
using MimeKit;
using Movie.Core.Helpers;
using Movie.Data.Contexts;
using Movie.Entity.Concrete.Mail;
using Movie.Entity.Concrete.Movies;
using Movie.Services.Abstract;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Text.Json;
using System.Threading.Tasks;
namespace Movie.Services.Concrete
{
    public class MovieService : IMovieService
    {
        #region ctor

        private readonly Context _context;
        private readonly IHttpClientFactory _httpClientFactory;
        private readonly MailSettings _mailSettings;
        private readonly MovieDbInfo _movieDbInfo;
        public MovieService(Context context, IHttpClientFactory httpClientFactory, IOptions<MailSettings>mailSettings, IOptions<MovieDbInfo> movieDbInfo) 
        {
            _context = context;
            _httpClientFactory = httpClientFactory;
            _mailSettings = mailSettings.Value;
            _movieDbInfo = movieDbInfo.Value;
        }
        #endregion

        #region Filme Not ve Puan Ekleme
        public async Task<MovieEntity> AddDetailAsync(MovieEntity movieEntity)
        {
           await _context.Movies.AddAsync(movieEntity);
            _context.SaveChanges();
            return movieEntity;

        }
        #endregion

        #region Film Tavsiye Etme
        public async Task<object> AddRecommendationsAsync(string recipientEmail, string recipientFirstName, int id)
        {
            HttpClient httpClient = _httpClientFactory.CreateClient();
            httpClient.BaseAddress = new Uri(_movieDbInfo.Url);
            var uri = id + "/recommendations?api_key=4f4107f0ab25c2318bfe2beacb0886e0&language=en-US&page=1";
            var result = await httpClient.GetStringAsync(uri);
            var fin = JObject.Parse(result);

            var email = new MimeMessage();
            email.From.Add ( MailboxAddress.Parse(_mailSettings.Mail));
            email.To.Add(MailboxAddress.Parse(recipientEmail));
            email.Subject ="Film Tavsiye Etme";
            //var builder = new BodyBuilder();
            // builder.HtmlBody = mailEntity.Body;
            email.Body = new TextPart(uri)
            {
                Text = fin.ToString()
            };

            var client = new SmtpClient();            
            try
            {
                await client.ConnectAsync(_mailSettings.Host, _mailSettings.Port, true);
                await client.AuthenticateAsync(new NetworkCredential(_mailSettings.Mail, _mailSettings.Password));
                await client.SendAsync(email);
                await client.DisconnectAsync(true);
                return "Mail Gönderimi Başarılı";
            }
            catch(Exception ex)
            {
                throw ex;
            
            }
            finally
            {
                client.Dispose();
            }
        }
        #endregion

        #region Film Listesi - Sayfa Sayfa
        public async Task<IEnumerable<MovieEntity>> GetListsAsync()
        {
            return await _context.Movies.ToListAsync();
        }

        #endregion

        #region Id ile Film Görüntüleme - ortalama puan, kullanıcının verdiği puan ve eklediği notlar
        public async Task<IEnumerable<MovieEntity>> GetMovieDetailAsync(int id)
        {
            return await _context.Movies.Where(x => x.MovieId == id).ToListAsync();

        }

        #endregion



    }
}
