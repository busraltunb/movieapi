﻿using Movie.Entity.Concrete.Mail;
using Movie.Entity.Concrete.Movies;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Movie.Services.Abstract
{
    public interface IMovieService 
    {
        Task<MovieEntity> AddDetailAsync(MovieEntity movieEntity); // Not ve Puan gibi Ekleme    
        Task<object> AddRecommendationsAsync(string recipientEmail, string recipientFirstName, int id); // mail adrese  tavsiye ekleme
        Task<IEnumerable<MovieEntity>> GetListsAsync(); //Sayfa sayfa listeleme
        Task<IEnumerable<MovieEntity>> GetMovieDetailAsync(int id); // Id ile  Görüntüleme

    }
}
