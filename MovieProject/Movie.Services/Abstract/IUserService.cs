﻿using Movie.Data.Dtos.UserDto;
using Movie.Entity.Concrete.User;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Movie.Services.Abstract
{
    public interface IUserService
    {
        Task<UserEntity> AddAsync(UserEntity userEntity);
        UserLogin Authenticate (string UserNicName, string UserPassword);

    }
}
