﻿using FluentValidation.AspNetCore;
using Hangfire;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.HttpsPolicy;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Microsoft.IdentityModel.Tokens;
using Microsoft.OpenApi.Models;
using Movie.Core.Helpers;
using Movie.Core.Validation;
using Movie.Data.Contexts;
using Movie.Entity.Concrete.User;
using Movie.Hangfire.Abstract;
using Movie.Hangfire.Concrete;
using Movie.Services.Abstract;
using Movie.Services.Concrete;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Text.Json.Serialization;
using System.Threading.Tasks;

namespace Movie.WebAPI
{
    public class Startup
    {
        #region ctor
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }
       

        public IConfiguration Configuration { get; }
        #endregion

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            #region Database Connection
            services.AddDbContext<Context>(options => options.UseSqlServer(Configuration.GetConnectionString("SqlConnection"),
                                           options => options.MigrationsAssembly(Assembly.GetAssembly(typeof(Context)).GetName().Name)));
            #endregion

            #region MailSetting

            services.Configure<MailSettings>(Configuration.GetSection("MailSettings"));

            #endregion

            #region MovieDbInfo
            services.Configure<MovieDbInfo>(Configuration.GetSection("MovieDbInfo"));
            #endregion

            #region Redis Connection

            //TODO : Startu - Redis için konfigürasyonu düzenle
            services.AddStackExchangeRedisCache(action =>
            {
                action.Configuration = "";
                {
                    Configuration.GetValue<string>("RedisSettings : Uri");
                    Configuration.GetValue<int>("RedisSettings : Port");
                    
                }
            });

            #endregion

            #region Secret Key Connection
            //services.Configure<AppSettings>(Configuration.GetSection("AppSettings"));
            var appSettingSection = Configuration.GetSection("AppSettings");
            services.Configure<AppSettings>(appSettingSection);
            var appSettings = appSettingSection.Get<AppSettings>();
            #endregion

            #region Jwt

            var key = Encoding.ASCII.GetBytes(appSettings.SecretKey); 
            services.AddAuthentication(x =>
            {
                x.DefaultAuthenticateScheme = JwtBearerDefaults.AuthenticationScheme;
                x.DefaultChallengeScheme = JwtBearerDefaults.AuthenticationScheme;
            }).AddJwtBearer(x =>
            {
                x.RequireHttpsMetadata = false;
                x.SaveToken = true;
                x.TokenValidationParameters = new TokenValidationParameters()
                {
                    ValidateIssuerSigningKey = true,
                    IssuerSigningKey = new SymmetricSecurityKey(key),
                    ValidateIssuer = false,
                    ValidateAudience = false
                };
            });

            #endregion

            #region Fluent Validation
            services.AddFluentValidation(x => x.RegisterValidatorsFromAssemblyContaining<MovieValidator>());
            services.AddFluentValidation(x => x.RegisterValidatorsFromAssemblyContaining<UserValidator>());
            #endregion

            #region Swagger
            services.AddSwaggerGen(c =>
            {
                #region ContactInfo
                c.SwaggerDoc("v1", new OpenApiInfo

                {
                    Title = "Movie.WebAPI",
                    Version = "v1",
                    Description = "Movie API With JWT-Hangfire",
                    Contact = new OpenApiContact
                    {
                        Name = "Büşra Altun",
                        Email = "busraltunb@gmail.com",
                        Url = new Uri("https://www.linkedin.com/in/busraaltun/"),
                    },
                });
                #endregion

                #region AuthorizeInfo
                c.AddSecurityDefinition("Bearer", new OpenApiSecurityScheme
                {
                    Description = "JWT Authorization header using the Bearer scheme. Example: \"Bearer 12345abcdef\"",
                    Name = "Authorization",
                    In = ParameterLocation.Header,
                    Type = SecuritySchemeType.ApiKey,
                    Scheme = "Bearer"
                });


                c.AddSecurityRequirement(new OpenApiSecurityRequirement
                {
                    {
                        new OpenApiSecurityScheme
                        {
                            Reference = new OpenApiReference
                            {
                                Type = ReferenceType.SecurityScheme,
                                Id = "Bearer"
                            },
                            Scheme = "oauth2",
                            Name = "Bearer",
                            In = ParameterLocation.Header,
                        },
                        new List<string>()
                    }
                });
#endregion

            });
            #endregion

            #region Scoped Services
            services.AddScoped<IUserService, UserService>();
            services.AddScoped<IMovieService, MovieService>();
            services.AddScoped<IJobTestService, JobTestService>();
            #endregion

            #region HangFire Connection

            services.AddHangfire(x =>
            {
                x.UseSqlServerStorage(Configuration.GetConnectionString("HangfireConnection"));
            });
            services.AddHangfireServer();

            #endregion


            services.AddHttpClient();
            
            #region Controllers
            services.AddControllersWithViews().AddNewtonsoftJson(options =>
            options.SerializerSettings.ReferenceLoopHandling = Newtonsoft.Json.ReferenceLoopHandling.Ignore);

            services.AddControllers();
            #endregion
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
                
            }

            app.UseHttpsRedirection();

            app.UseSwagger(c => { c.SerializeAsV2 = true; });
            app.UseSwaggerUI(c => c.SwaggerEndpoint("/swagger/v1/swagger.json", "Movie.WebAPI v1"));

            app.UseRouting();

            app.UseAuthentication();

            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });

            app.UseHangfireDashboard();

        }
    }
}
