﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Movie.Core.Constants.Responses;

namespace Movie.WebAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class CustomBasesController : ControllerBase
    {
        [NonAction]
        protected IActionResult Success(Response data)
        {
            return Ok(data);
        }
        [NonAction]
        protected IActionResult Success<T>(DataResponse<T> data)
        {
            return Ok(data);
        }

        [NonAction]
        protected IActionResult Success(string message)
        {
            return Success(new Response
            {
                Success = true,
                Message = message
            });
        }

        [NonAction]
        protected IActionResult Success<T>(string message, T data)
        {
            return Success(new DataResponse<T>
            {
                Success = true,
                Message = message,
                Data = data
            });
        }
        [NonAction]
        protected IActionResult Created<T>(string message, T data)
        {
            return Created(new DataResponse<T>
            {
                Success = true,
                Message = message,
                Data = data
            });
        }
        [NonAction]
        protected IActionResult Created<T>(DataResponse<T> data)
        {
            return StatusCode(201, data);
        }
        [NonAction]
        protected IActionResult BadRequest<T>(string message, T data)
        {
            return BadRequest(new DataResponse<T>
            {
                Success = false,
                Message = message,
                Data = data
            });
        }
        [NonAction]
        protected IActionResult BadRequest<T>(DataResponse<T> data)
        {
            return StatusCode(400, data);
        }

        [NonAction]
        protected IActionResult NotFound<T>(string message, T data)
        {
            return NotFound(new DataResponse<T>
            {
                Success = false,
                Message = message,
                Data = data
            });
        }
        [NonAction]
        protected IActionResult NotFound<T>(DataResponse<T> data)
        {
            return StatusCode(404, data);
        }
        [NonAction]
        protected IActionResult Error<T>(string message, T data)
        {
            return Error(new DataResponse<T>
            {
                Success = false,
                Message = message,
                Data = data

            });
        }
        protected IActionResult Error<T>(DataResponse<T> data)
        {
            return StatusCode(500, data);
        }
    }

}
