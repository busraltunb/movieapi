﻿using Hangfire;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Movie.Core.Constants;
using Movie.Hangfire.Abstract;
using System.Threading.Tasks;

namespace Movie.WebAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class JobsController : CustomBasesController
    {
        #region ctor
        private readonly IJobTestService _jobTestService;
        private readonly IBackgroundJobClient _backgroundJobClient;
        private readonly IRecurringJobManager _recurringJobManager;

        public JobsController( IJobTestService jobTestService, IBackgroundJobClient backgroundJobClient , IRecurringJobManager recurringJobManager)
        {
            _jobTestService = jobTestService;
            _backgroundJobClient = backgroundJobClient;
            _recurringJobManager = recurringJobManager;
        }

        #endregion


        [HttpGet("/ReccuringJob")]
        public async Task<IActionResult> ReccuringJob()
        {
            //_recurringJobManager.AddOrUpdate("jobId", () => _jobTestService.ReccuringJob(), Cron.Minutely);
            var result = await _jobTestService.ReccuringJob();
            return Success(Messages.Success, result);

        }
    }
}
