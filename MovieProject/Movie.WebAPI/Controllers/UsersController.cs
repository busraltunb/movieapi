﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Caching.Distributed;
using Movie.Core.Constants;
using Movie.Data.Dtos.UserDto;
using Movie.Entity.Concrete.User;
using Movie.Services.Abstract;
using System.Threading.Tasks;

namespace Movie.WebAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class UsersController : CustomBasesController
    {
        
        #region ctor
        private readonly IUserService _userService;
        private readonly IDistributedCache _distributedCache;
        public UsersController(IUserService userService, IDistributedCache distributedCache)
        {
            _userService = userService;
            _distributedCache = distributedCache;
        }
        #endregion

        #region Create

        [HttpPost("Create", Name = " CreateUser")]
        public async Task<IActionResult> Create([FromBody] UserEntity userEntity)
        {
        
            var result = await _userService.AddAsync(userEntity);
            return Success(Messages.Success, result);
        }
        #endregion

        #region LOGİN
        /// <summary>
        /// user nick name ve şifre ile kimlik doğrulama
        /// </summary>
        /// <param name="userEntity"></param>
        /// <returns></returns>


        [AllowAnonymous]
        [HttpPost("authenticate", Name = "AuthenticateUser")]
        public IActionResult Authenticate([FromBody] UserLogin userLogin)
        {
            var userToken = _userService.Authenticate(userLogin.UserNickName, userLogin.UserPassword);
            return Success(Messages.Success, userToken.UserToken);
        }
        #endregion
    }
}
