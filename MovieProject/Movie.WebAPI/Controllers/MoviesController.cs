﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Movie.Core.Constants;
using Movie.Entity.Concrete.Mail;
using Movie.Entity.Concrete.Movies;
using Movie.Services.Abstract;
using System;
using System.Threading.Tasks;

namespace Movie.WebAPI.Controllers
{
   
    [Route("api/[controller]")]
    [ApiController]
    public class MoviesController : CustomBasesController
    {
        #region ctor
        private readonly IMovieService _movieService;
        public MoviesController(IMovieService movieService)
        {
            _movieService = movieService;
        }

        #endregion

        #region GET

        #region Id ile Film Listeleme EndPoint
        [AllowAnonymous]
        [HttpGet("movielist", Name = "GetMovieDetail")]
        public async Task<IActionResult> Get(int id)
        {
            var result = await _movieService.GetMovieDetailAsync(id);
            return Success(Messages.Success, result);
        }
        #endregion

        #region Film Listesi EndPoint

        [HttpGet("list", Name = "GetMovieList")]
        public async Task<IActionResult> Get()
        {
            var result = await _movieService.GetListsAsync();

            return Success(Messages.Success, result);
        }
        #endregion

        #endregion

        #region POST

        #region Not ve Puan Ekleme EndPoint
        [HttpPost("addMovieDetail", Name = "AddMovieDetail")]
        public async Task<IActionResult> AddDetail([FromBody] MovieEntity movieEntity)
        {
            //movieEntity.Value = (int)await _movieService.AddDetailAsync(movieEntity);
            var result = await _movieService.AddDetailAsync(movieEntity);

            return CreatedAtAction("Get", new {id = result.Id}, result);
        }
        #endregion

        #region Film Tavsiye Etme
        [HttpPost("sendMovieRecommendation", Name = "sendMovieRecommendationWithEmail")]
        

        public async Task<IActionResult> SendMovieRecommendation(string recipientEmail, string recipientFirstName, int id)
        {
            try
            {
                await _movieService.AddRecommendationsAsync(recipientEmail, recipientFirstName,id);
                return Success(Messages.Success);
            }

            catch(Exception ex)
            {
                return BadRequest(ex.Message.ToString());
            }
        }
        #endregion

        #endregion
    }
}
