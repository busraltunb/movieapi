﻿using Microsoft.EntityFrameworkCore;
using Movie.Data.Dtos.UserDto;
using Movie.Entity.Concrete;
using Movie.Entity.Concrete.Movies;
using Movie.Entity.Concrete.User;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace Movie.Data.Contexts
{
    public class Context : DbContext
    {
        #region ctor

        public Context(DbContextOptions<Context> options) : base(options)
        {

        }
        #endregion

        #region OnModelCreating
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            //Configuration ları haberdar ediyorum
            modelBuilder.ApplyConfigurationsFromAssembly(Assembly.GetExecutingAssembly());

            /*
              Tek bir tane configuration dosyasını çalıştırmak için;
             modelBuilder.ApplyConfigurationsFromAssembly(new BookConfiguration());
             */
            base.OnModelCreating(modelBuilder);
        }
        #endregion

        #region DbSet

        public DbSet<UserEntity> Users { get; set; }
        public DbSet<MovieEntity> Movies { get; set; }

        #endregion
    }
}
