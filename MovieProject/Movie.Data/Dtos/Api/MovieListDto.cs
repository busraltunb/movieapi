﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Movie.Data.Dtos.Api
{
    public class MovieListDto
    {
        public List<MovieDto> results { get; set; }
    }


    public class MovieDto
    {
        public long id { get; set; }
        public string title { get; set; }
        public decimal vote_average { get; set; }
        public string overview { get; set; }
    }
}
