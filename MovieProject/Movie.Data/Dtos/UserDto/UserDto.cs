﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.Json.Serialization;
using System.Threading.Tasks;

namespace Movie.Data.Dtos.UserDto
{
    public class UserDto
    {
        public int Id { get; set; }
        public string UserName { get; set; }
        public string UserLastname { get; set; }
        public string UserNickName { get; set; }
        public string UserMailAddress { get; set; }

        
        //[JsonIgnore]
        public string UserPassword { get; set; }
    }
}
