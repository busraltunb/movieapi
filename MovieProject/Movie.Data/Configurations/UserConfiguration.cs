﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Movie.Entity.Concrete.User;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Movie.Data.Configurations
{
    public class UserConfiguration : IEntityTypeConfiguration<UserEntity>
    {
        public void Configure(EntityTypeBuilder<UserEntity> builder)
        {
            builder.HasKey(u => u.Id);

            builder.Property(u => u.Id)
                   .ValueGeneratedOnAdd();

            builder.Property(u => u.UserName)
                    .IsRequired()
                    .HasMaxLength(100);

            builder.Property(u => u.UserLastname)
                   .IsRequired()
                   .HasMaxLength(100);

            builder.Property(u => u.UserNickName)
                   .IsRequired()
                   .HasMaxLength(50);

            builder.Property(u => u.UserPassword)
                   .IsRequired()
                   .HasMaxLength(16);
            
            builder.Property(u => u.UserMailAddress)
                   .IsRequired()
                   .HasMaxLength(200);

            builder.HasMany(x => x.Movies)
                   .WithOne(x => x.User)
                   .HasForeignKey(x => x.UserId);



        }

    }
}
