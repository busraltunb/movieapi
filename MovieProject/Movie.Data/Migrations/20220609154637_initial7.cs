﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Movie.Data.Migrations
{
    public partial class initial7 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameColumn(
                name: "MovieRating",
                table: "Movies",
                newName: "Value");

            migrationBuilder.RenameColumn(
                name: "MovieName",
                table: "Movies",
                newName: "Title");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameColumn(
                name: "Value",
                table: "Movies",
                newName: "MovieRating");

            migrationBuilder.RenameColumn(
                name: "Title",
                table: "Movies",
                newName: "MovieName");
        }
    }
}
