﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Movie.Data.Migrations
{
    public partial class initial4 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "UserEntityId",
                table: "UserLogins",
                type: "int",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_UserLogins_UserEntityId",
                table: "UserLogins",
                column: "UserEntityId");

            migrationBuilder.AddForeignKey(
                name: "FK_UserLogins_Users_UserEntityId",
                table: "UserLogins",
                column: "UserEntityId",
                principalTable: "Users",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_UserLogins_Users_UserEntityId",
                table: "UserLogins");

            migrationBuilder.DropIndex(
                name: "IX_UserLogins_UserEntityId",
                table: "UserLogins");

            migrationBuilder.DropColumn(
                name: "UserEntityId",
                table: "UserLogins");
        }
    }
}
