﻿using Movie.Core.Constants.Responses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Movie.Core.Constants.Errors
{
    public class Error : Response
    {
        public string InnerMessage { get; set; }

    }
}
