﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Movie.Core.Constants
{
    public class Messages
    {
        public static string Success = "The operation  successfull";
        public static string GeneralError => "Something went wrong. Please try again later.";
        public static string NotFound => "Item not found";
        public static string InvalidRequest => "Invalid Request";
        public static string AddingSuccess => "Adding is successful";
        public static string AddingFailed => "Adding is failed!";
        public static string UpdatingSuccess => "Updating is successful";
        public static string UpdatingFailed => "Updating is failed!";
        public static string DeletingSuccess => "Deleting is successful";
        public static string DeletingFailed => "Deleting is failed!";
    }
}
