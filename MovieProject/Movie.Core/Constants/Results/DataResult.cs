﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.Json.Serialization;
using System.Threading.Tasks;

namespace Movie.Core.Constants.Results
{
    public class DataResult<T> : Result
    {
        [JsonPropertyName("data")]
        public T Data { get; set; }
    }
}
