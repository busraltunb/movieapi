﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Movie.Core.Constants.Responses
{
    public class DataResponse<T> : Response
    {
        public T Data { get; set; }
    }
}
