﻿using FluentValidation;
using Movie.Entity.Concrete.Movies;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Movie.Core.Validation
{
    public class MovieValidator : AbstractValidator<MovieEntity>
    {
        public MovieValidator()
        {
            RuleFor(m => m.Value).NotEmpty().WithMessage("Your rating for movie can not be emty");                
        }
    }

}
