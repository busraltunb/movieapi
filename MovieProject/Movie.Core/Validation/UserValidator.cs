﻿using FluentValidation;
using Movie.Entity.Concrete.User;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Movie.Core.Validation
{
    public class UserValidator : AbstractValidator<UserEntity>
    {
        public UserValidator()
        {
            
            RuleFor(u => u.UserNickName).NotNull().NotEmpty().WithMessage("Your nick name cannot be empty")
                   .MinimumLength(8).WithMessage("Your password length must be at least 8.")
                   .MaximumLength(50).WithMessage("Your password length must not exceed 50.");

            RuleFor(u => u.UserMailAddress).EmailAddress().NotNull().NotEmpty().WithMessage("Maill address can not be empty");

            RuleFor(u => u.UserPassword).NotEmpty().NotNull().WithMessage("Your password cannot be empty")
                    .MinimumLength(8).WithMessage("Your password length must be at least 8.")
                    .MaximumLength(16).WithMessage("Your password length must not exceed 16.")
                    .Matches(@"[A-Z]+").WithMessage("Your password must contain at least one uppercase letter.")
                    .Matches(@"[a-z]+").WithMessage("Your password must contain at least one lowercase letter.")
                    .Matches(@"[0-9]+").WithMessage("Your password must contain at least one number.")
                    .Matches(@"[\!\?\*\.]+").WithMessage("Your password must contain at least one (!? *.).");
        }
    }
}
