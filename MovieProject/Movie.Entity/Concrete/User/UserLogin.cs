﻿using Movie.Entity.Abstract;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Movie.Entity.Concrete.User
{
    /// <summary>
    /// Login için kullanıcıdan istenen bilgileri içerir
    /// Nick name ve şifre alanı zorunludur
    /// </summary>
    public class UserLogin : BaseEntity
    {
        public string UserNickName { get; set; }
        public string UserPassword { get; set; }

        public string UserToken { get; set; }
        public UserEntity UserEntity { get; set; }

    }
}
