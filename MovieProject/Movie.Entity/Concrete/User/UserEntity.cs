﻿using Movie.Entity.Abstract;
using Movie.Entity.Concrete.Movies;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.Json.Serialization;
using System.Threading.Tasks;

namespace Movie.Entity.Concrete.User
{
    public class UserEntity : BaseEntity
    {
        public string UserName { get; set; }
        public string UserLastname { get; set; }
        public string UserNickName { get; set; }
        public string UserMailAddress { get; set; }

       // [JsonIgnore]
        public string UserPassword { get; set; }

        //Navigation Property
        public ICollection<MovieEntity> Movies { get; set; }
        

    }
}
