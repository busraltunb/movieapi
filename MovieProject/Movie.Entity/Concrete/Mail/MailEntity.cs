﻿using Movie.Entity.Abstract;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Movie.Entity.Concrete.Mail
{
    public class MailEntity : BaseEntity
    {
      
        public string ToEmail { get; set; }
        public string Subject { get; set; }
        public string Body { get; set; }
        public string Link { get; set; }
    }
}
