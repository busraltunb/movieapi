﻿using Movie.Entity.Abstract;
using Movie.Entity.Concrete.User;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Movie.Entity.Concrete.Movies
{
    public class MovieEntity : BaseEntity
    {
        public long MovieId { get; set; }
        public int? UserId { get; set; }
        public string Title { get; set ; }

        [Range(1,10)]
        public int Value  { get; set ; }
        public string MovieNote { get; set ; }   
        public UserEntity User { get; set; }

    }
}
