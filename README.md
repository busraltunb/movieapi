# PROJE KONUSU ve İSTENİLENLER
.Net5 ya da.Net6 ile basit bir film tavsiye uygulaması geliştirmeni istiyoruz. 
• Uygulama sadece bir API hizmeti olacak. Herhangi bir arayüz olmasına gerek yok.
• API kimlik doğrulaması gerektirecek. Bunun icin Auth0 veya benzeri bir servis kullanabilirsin. Ya da JWT oluşturan bir login metodu geliştirebilirsin.
• API şu endpoint' leri sunacak: 
• Film listesi: Sayfa sayfa tüm filmleri alabileceğimiz bir endpoint. Sayfa büyüklüğü parametre olarak alınacak.
• Id ile film görüntüleme: Film bilgileri ile birlikte ortalama puan, kullanıcının verdiği puan ve eklediği notlar gösterilecek.
• Filme not ve puan ekleme: Not text alanı olacak. Puan ise 1-10 arası bir tam sayı olmak zorunda. 
• Film tavsiye etme: Verilen bir e-posta adresine mail atılacak.
 
• Arka planda çalışan bir uygulama ile film listesi periyodik olarak (örn. saat başı) themoviedb.org'dan (https://developers.themoviedb.org/3/getting-started/introduction) çekilecek. 
• Kodu tüm listeyi çekecek şekilde yazmalısın ama tüm liste çok büyük olacağı için maksimum film sayısı belirleyip sınırlayabilirsin.
• Eklenen metotlar için birim testleri yazabilirsin.
• API dokümantasyonu yapabilirsin.
• İstersen Docker, Redis, RabbitMQ gibi teknolojileri kullanabilirsin.


# PROJE BAŞLANGIÇ

* Projeye .NET 5 kullanarak geliştirme yapacağımı belirledim.
* ORM olarak Entity Framework ü tercih ettim çünkü Code First yaklaşımıyla projeyi gerçekleştirecektim. Bu noktada da Fluent Validation Configurationlar kullandım. 

# Katman İsmi-Kullanılan Teknolojiler

CORE : Fluent Validation kullanıldı.
DATA : Entity Framework kullanıldı.
ENTİTY : 
HANGFİRE : Hangfire kullanıldı
SERVİCES : AutoFac, MailKit, MimeKit kullanıldı
WEB API : Entity Framework, JwtBearer, Identity, Swagger kullanıldı.
Veri Tabanı : SQL Server

# Services

* MovieService

Film listesi (sayfa)
![FilmListesi](/images/filmlistesisayfa.PNG)

Id ile film görüntüleme (detaylar)
<img src="/images/idilefilm.PNG" width="auto">

Filme not ve puan ekleme(not ve puan)
<img src="/images/filmenotvepuanekleme.PNG" width="auto">

Film tavsiye etme: Verilen bir e-posta adresine mail atılacak.
<img src="/images/filmtavsiyeetme.PNG" width="auto">


Mail gönderme işleminde "düşük güvenli uygulamalara izin ver" sorunu ile karşılaştım.

<img src="/images/google.PNG" width="auto">

* UserServices
Login işlemi ve kullanıcı ekleme işlemi 

* JobsServices
Hangfire ile ile film listesini saat başı themoviedb.org'dan (https://developers.themoviedb.org/3/getting-started/introduction) çekmek için serviste bu şekilde işlem gerçekleştirdim 
<img src="/images/hangfire.PNG" width="auto">


* Docker indirdim biraz üzerinde uğraştım.

<img src="/images/docker.PNG" width="auto">

* Docker üzerinde Redis kurmakla ilgilendim. Redis i projeye entegrasyonunu henüz gerçekleştirmedim.

# Startup.cs

<img src="/images/startup.PNG" width="auto">

# Swagger

<img src="/images/specialswagger.PNG" width="auto">


# Son

<img src="/images/swagger.PNG" width="auto">

